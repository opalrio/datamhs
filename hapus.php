<?php
session_start();
// cek sessin
if (!isset($_SESSION["login"])) {
  header("Location: login.php");

  exit;
}

require 'functions.php';

$id = $_GET["id"];

if (hapus($id) > 0) {
  echo "
     <script>
        alert('data berhasil dihapus!');
        document.location.haref = 'index.php';    
     </script>  
     ";
  header("Location: index.php");
  exit;
} else {
  echo "<script>
    alert('data gagal dihapus!');
    document.location.haref = 'index.php';    
 </script>    ";
  header("Location: index.php");
  exit;
}
